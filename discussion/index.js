// console.log("Hello World!");

//single line comment
/*multi line comment*/

/*let productName = 'desktop computer' //can be change
					//Camel Case
const	//cannot be changed
*/


/*let productName = 'desktop computer';
console.log(productName);
productName = 'cellphone';
console.log(productName);s

let productPrice = 500;
console.log(productPrice);
productPrice = 1500;
console.log(productPrice);*/


// CONSTANT ********************************
/*
 values that do not change
*/

// const deliveryFee = 30;
// console.log(deliveryFee);
// deliveryFee = 45;


//DATA TYPES ************************************
// 1. String - characters

let country = 'Ph';
let province = 'Mnl';
console.log(country, province); //Concatenation plus sign


// 2. numbers - integers

let headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

let PI = 22/7;
console.log(PI);
console.log	(Math.PI);

console.log("John's grade is: " + grade);

// 2. BOOLEAN  - logical values true or false

let isMarried = false;
let isGoodConduct = true;
console.log(isMarried);
console.log(isGoodConduct);

// 4. OBJECTS

// Arrays


let grades = [99.8, 92.1, 90.2, 75.5, 78.0];
console.log(grades[1]);



// OBJECT LITERALS
	// propety of the object
//  sytnax : let/const objectname = {propertyA: value, propertyB: value }


let personDetails = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	status: false,
	contact: ['+639871234324', '2142314234'],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}
console.log(personDetails);